<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * Tarjetas
 */
Route::get('tarjeta/listTarjetas', 'Tarjetas\TarjetaController@index');
Route::post('tarjeta/crearTarjeta', 'Tarjetas\TarjetaController@store');
Route::delete('tarjeta/eliminarTarjeta', 'Tarjetas\TarjetaController@destroy');
Route::get('tarjeta/getTarjetasSelect', 'Tarjetas\TarjetaController@getTarjetasSelect');
/**
 * Clientes
 */
Route::get('clientes/listClientes', 'Clientes\ClienteController@index');
Route::post('clientes/crearCliente', 'Clientes\ClienteController@store');
Route::get('clientes/edit/{id}', 'Clientes\ClienteController@edit');
Route::get('clientes/getExcel', 'Clientes\ClienteController@export');
Route::get('clientes/listTarjAdicio', 'Clientes\ClienteController@tarjetasAdicionales');
