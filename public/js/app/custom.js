function showToast(text, tipo = 1) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    if(tipo == 1){
        x.style.backgroundColor = "#d1ecf1";
        x.style.color = "#0c5460";
        x.style.borderColor = "#bee5eb";
    }

    if(tipo == 2){
        x.style.backgroundColor = "#f8d7da";
        x.style.color = "#721c24";
        x.style.borderColor = "#f5c6cb";
    }

    x.innerHTML = text;

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
  }

  function opcionesFormatter(value, row, index, field) {
      return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a>';
  }

  function opcionesFormatterEval(value, row, index, field) {
    return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a> / <a href="'+window.location.href+'/listEstudiantes/'+row.id+'/'+row.tipo+'">Resultados</a>';
  }

  function comentarioEva(value, row, index, field){
    let comentario = String(row.comentario);
    return '<a href="#" class="btn btn-link" Onclick="verComentario(\''+comentario+'\')">Ver comentario</a>';
  }

  function opcionesTipoEval(value, row, index, field) {
    let tipo = "Formulario";
    if(row.tipo == 2)
        tipo = "Apreciativa";

    return tipo
  }

  function opcionesCursosFormatter(value, row, index, field) {
      return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a> / <a href="'+window.location.href+'/programacion/'+row.id+'">Editar programacion</a> / <a href="'+window.location.href+'/calificacionEstudiantes/'+row.id+'">Calificación estudiantes</a>';
  }

  function opcionesFormatterFile(value, row, index, field) {
    return '<a href="../api/archivos/leerArchivo/'+row.id+'/0/" target="_blank">Ver</a> / <a href="../api/archivos/leerArchivo/'+row.id+'/1/">Descargar</a>';
  }

  function opcionesProveedorFormatter(value, row, index, field) {
      return '<a href="#" onClick="return deleteItem('+row.id+');" data-id="'+row.id+'">Eliminar</a> / <a href="'+window.location.href+'/form/'+row.id+'">Editar</a> / <a href="'+window.location.href+'/dashboardproveedor/'+row.id+'">Ver perfil del proveedor</a> / <a href="'+window.location.href+'/calificar/'+row.id+'">Calificar proveedor</a>';
  }

  function deleteFormatter(value, row, index, field) {
    return '<a href="#" onClick="return deleteItemsPlantilla('+row.id+');" data-id="'+row.id+'">Eliminar</a>';
  }

  function deleteMatriculaFormatter(value, row, index, field) {
    return '<a href="#" onClick="return deleteMatricula('+row.id+');" data-id="'+row.id+'">Eliminar</a>';
  }

  function fechaFormartter (value, row, index, field){
      return ''+row.ano+'/'+row.mes+'';
  }

  function dataPerfil(value, row, index, field) {
    return '<a href="'+window.location.href+'/perfil/'+row.correo+'/1">Ver perfil del estudiante</a>';
  }

  function perfilCursosFormatter(value, row, index, field) {
    return '<a href="../../../cursos/perfil/'+row.id+'">Ver información del curso</a>';
  }

  function optionPreguntasFormatter(value, row, index, field) {
      let display = "initial";
      if(row.tipo == 1 ){
        display = "none";
      }
    return '<a href="#" onClick="return deletePregunta('+row.id+');" data-id="'+row.id+'">Eliminar</a> <div style="display:'+display+';" >/ <a href="#"onClick="return modalAddPreguntas('+row.id+','+row.tipo+');">Administrar respuestas</a></div>';
  }

  function eliminarRespuestaFormatter(value, row, index, field) {
    return '<a href="#" onClick="return deleteRespuesta('+row.id+');" data-id="'+row.id+'">Eliminar</a>';
  }

  function validarCorrectaFormatter(value, row, index, field) {
      if(row.correcta == 1){
        return "Si";
      }else{
        return "No";
      }
  }

  function booleanFormatter(value) {
      if(value == 1){
        return "Si";
      }else{
        return "No";
      }
  }

  function estadoFormartter (value, row, index, field){

      let estado = "";

      if(row.estado == 0)
         estado = "Espera";
      if (row.estado == 1)
          estado = "Aprobada";
      if (row.estado == 2)
          estado = "Rechazada";

      return estado;
  }

  function linkFormatter (value, row, index, field) {

    return '<a href="'+row.url+'" target="_blank">Ver pdf</a>';
  }

  function estadoPaFormartter (value, row, index, field){

      let estado = "";

      if(row.estadoplandeaccion == 0)
         estado = "Espera";
      if (row.estadoplandeaccion == 1)
          estado = "Aprobada";
      if (row.estadoplandeaccion == 2)
          estado = "Rechazada";

      return estado;
  }

  function opcionesCalificacionFormatter(value, row, index, field){

      let editar = '<a href="../calificaciones/calificar/'+row.proveedore_id+'/1">Editar</a>';
      let aprobar = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',1)">Aprobar</a>';
      let rechazar = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',2)">Rechazar</a>';
      let planAccion = '<a href="#" onClick="cambiarEstadoCalificacion('+row.id+',3)">Aprobar plan de accion</a>'

      let opciones = "";

      if(row.estado == 0)
          opciones = ''+aprobar+' / '+rechazar+'';

      if(row.estado == 2)
          opciones = ''+aprobar+' / '+editar+''

      if(row.plandeaccion != "" && row.estadoplandeaccion == 0 && row.estado != 1)
          opciones += ' / '+planAccion+'';

      return opciones;
  }

  function tipoFormatter(value, row, index, field){

    if(row.tipo == 1)
        return "Turno Mixto";

    if(row.tipo == 2)
        return "Capacitacion Interna";

    if(row.tipo == 3)
        return "Capacitacion Externa";
  }


function deleteItem(id) {

    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: 'api/'+basecrud+'/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                console.log(data);
                jQuery('#table').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }

    return false;
}


function deleteItemsPlantilla(id) {
    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: '../../api/itemsPlantillas/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                jQuery('#table_item').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }
    return false;
}

function deleteMatricula(id) {
    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: '../../api/matriculas/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                jQuery('#table_item').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }
    return false;
}

function cambiarEstadoCalificacion(id,estado){
  jQuery.ajax({
      url: 'api/calificaciones/cambiarEstados/'+id+'/'+estado,
      type: 'GET',
      success:function(data) {
        showToast(data.message);
        jQuery('#table').bootstrapTable('refresh');
      },
      error: function(data) {
        showToast(data.message);
      }
  });

  return false;
}


function mostrarIframeFile(id) {
    jQuery('#frame_file').attr('src', '../api/archivos/leerArchivo/'+id+'/0/');
    jQuery('#verFile').modal('show');

    return false;
}

function llamarModalAlertError(message){
    $("#textAlert").html(message);
    $("#modalAlert").modal();
}

function ShowAlert(){
    $('#table').on('load-success.bs.table', function (e, data) {
        if(data.status == 403){
            llamarModalAlertError(data.message);
        }
    });
}

function perfilCurso (id_curso){
    alert("Falta por definir esta funcionalidad");
}

function deletePregunta(id) {
    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: '../../api/preguntas/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                jQuery('#table_item').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }
    return false;
}


function deleteRespuesta(id) {
    if (confirm('Esta seguro de eliminar este registro?')){
        jQuery.ajax({
            url: '../../api/opcionesPreguntas/destroy/'+id,
            type: 'DELETE',
            success: function(data) {
                showToast(data.message);
                jQuery('#table_respuestas').bootstrapTable('refresh');
            },
            error: function(data) {
                showToast(data.message);
            }
        });
    }
    return false;
}

function modalAddPreguntas(id_pregunta, tipo_pregunta) {
    console.log(id_pregunta);
    console.log(tipo_pregunta);

    $("#pregunta_id").val(id_pregunta);
    $("#tipo_pregunta").val(tipo_pregunta);

    if(tipo_pregunta == 1){
        $("#titleModal").html("Ingresar respuesta tipo Abierta");
        $("#abierta").css("display","initial");
        $("#Seleccion").css("display","none");
    }

    if(tipo_pregunta == 2){
        $("#titleModal").html("Ingresar respuesta tipo Seleccion multiple");
        $("#Seleccion").css("display","initial");
        $("#abierta").css("display","none");
    }

    if(tipo_pregunta == 3){
        $("#titleModal").html("Ingresar respuesta tipo Seleccion unica");
        $("#Seleccion").css("display","initial");
        $("#abierta").css("display","none");
    }
    $('#table_respuestas').bootstrapTable('refresh');
    jQuery('#addPreguntas').modal('show');
    return false;
}

function verComentario(comentario){
    $("#textComentario").html(comentario);
    $("#modalComentario").modal('show');
}

