$(function () {

    /**
     * Carga los botones de la tabla
     */
    opcionesClientesFormatter = function (value, row, index, field) {

        var html = "";
        html = `<div class="d-flex p-2 bd-highlight">`;
        html += '<button type="button" class="btn btn-primary button-opt optionFormatter mr-2" onclick="varflotilla(\'' + row.nombre + '\', \'' + row.numero +'\','+row.id+',);" title="ver flotilla"><i class="fas fa-eye"></i></button>';
        html += `<button type="button" class="btn btn-warning button-opt optionFormatter" onclick="cargarDatosEditarCliente(${row.id});" title="editar el cliente"><i class="fas fa-edit"></i></button>`;

        html += `</div>`;

        return html;
    }

    /**
     * Metodo para crear el select de tarjetas
     */
    getTarjetasSelect = function (flotilla = 0, id_cliente) {
        $("#tarjeta").select2({
            minimumResultsForSearch: 1,
            allowClear: true,
            placeholder: "Seleccione una tarjeta",
            width: "100%",
            ajax: {
                url: "api/tarjeta/getTarjetasSelect",
                method: "GET",
                data: function (params) {
                    return {
                        serach: params.term,
                        flotilla: flotilla,
                        id_cliente: id_cliente
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (val, i) {
                            return { id: val.id, text: val.number };
                        })
                    };
                }
            }
        });
    }

    $("#flota").click(function () {
        getTarjetasSelect($("#flota").val());
        $("#tarjeta")
            .val(null)
            .trigger("change");
    });

    $("#createClient").click(function () {
        $("#dataCliente")[0].reset();
        $("#id_cliente_edit").val(0);
        getTarjetasSelect($("#flota").val());
        $("#tarjeta")
            .val(null)
            .trigger("change");
        $("#clienteModal").modal('show');
    });

    /**
     * Envia los datos del formulario a las APIS
     */
    jQuery("#dataCliente").submit(function (event) {
        event.preventDefault();

        jQuery.ajax({
            url: "api/clientes/crearCliente",
            type: "POST",
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            data: jQuery('#dataCliente').serialize(),
            success: function (data) {
                if (data.success) {
                    alert(data.message);
                    $("#clienteModal").modal("hide");
                    $("#table_cliente").bootstrapTable('refresh');
                } else {
                    alert(data.message);
                    $("#clienteModal").modal("hide");
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    /**
     * Carga los datos del cliente para editar
     * @param ins $id
    */
    cargarDatosEditarCliente =function (id) {
        jQuery.ajax({
            url: "api/clientes/edit/" + id,
            type: "GET",
            success: function (data) {
                console.log(data);
                jQuery("#dataCliente")[0].reset();
                $("#id_cliente_edit").val(data.id);
                $("#nombre").val(data.nombre);
                $("#phone").val(data.phone);
                $("#email").val(data.correo);
                $("#sexo").val(data.sexo);
                $("#flota").val(data.flota);
                $("#rfc").val(data.RFC);
                getTarjetasSelect(data.flota, data.id);
                data.tarjetas.forEach(element => {
                    $("#tarjeta")
                        .data("select2")
                        .trigger("select", {
                            data: { id: element.id, text: element.number }
                        });
                });
                $("#estatus").val(data.estatus);
            },
            error: function (data) {
                console.log(data.message);
            }
        });
        $("#clienteModal").modal("show");
    }


    $("#estatus").click(function () {
        $("#table_cliente").bootstrapTable('refresh');
    })

    $("#excel").click(function () {
        alert("Descargando Excel. Por favor espero unos segundos");
        location.href = "/api/clientes/getExcel";
    });

    varflotilla = function (nombre, numero, id) {
        $("#clienteFlot").html(nombre);
        $("#numero").html(numero);
        $("#id_cliente_search").val(id);
        $("#table_flotilla").bootstrapTable('refresh');
        $("#flotillaModal").modal('show');
    }

});

function queryParamsTableCliente(params) {
    return {
        search: params.search,
        order: params.order,
        offset: params.offset,
        limit: params.limit,
        estatus: $("#estatus").val()
    };
}

function queryParamsTableFlotilla(params) {
    return {
        cliente: $("#id_cliente_search").val()
    };
}
