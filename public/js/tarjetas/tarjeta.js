$(function () {

    /**
     * Pinta los botones de la tabla
     */
    opcionesTarjetasFormatter = function (value, row, index, field) {
        var html = "";
        html = `<div class="d-flex p-2 bd-highlight">`;
        html += `<button type="button" class="btn btn-danger button-opt optionFormatter" onclick="eliminarTarjeta(${row.id});" title="Eliminar tarjeta"><i class="fa fa-trash"></i></button>`;
        html += `</div>`;

        return html;
    }

    /**
     * LLama la modal para crear la tarjeta
     */
    $("#createTarjet").click(function () {
        $("#dataTarjeta")[0].reset();
        $("#tarjetaModal").modal('show');
    });

    /**
     * Envia los datos del formulario a las APIS
     */
    jQuery("#dataTarjeta").submit(function (event) {
        event.preventDefault();

        jQuery.ajax({
            url: "api/tarjeta/crearTarjeta",
            type: "POST",
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
            data: jQuery('#dataTarjeta').serialize(),
            success: function (data) {
                if (data.success){
                    alert(data.message);
                    $("#tarjetaModal").modal("hide");
                    $("#table_tarjeta").bootstrapTable('refresh');
                } else {
                    alert(data.message);
                    $("#tarjetaModal").modal("hide");
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

    /**
     * Eliminar una tarjeta
     * @param int id
     */
    eliminarTarjeta = function (id) {
        if (confirm("Esta seguro de eliminar este tarjeta")) {
            jQuery.ajax({
                url: "api/tarjeta/eliminarTarjeta",
                method: "DELETE",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                data: { id: id },
                success: function (data) {
                    if (data.success) {
                        alert(data.message);
                        $("#table_tarjeta").bootstrapTable('refresh');
                    } else {
                        alert(data.message);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    }

});
