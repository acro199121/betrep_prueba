<div class="modal fade" id="tarjetaModal" tabindex="-1" role="dialog" aria-labelledby="tarjetaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ingresar nuevo tarjeta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form enctype="multipart/form-data" id="dataTarjeta" method="post" >
            <div class="modal-body mbsr">
                {{ csrf_field() }}

                <p><b>Los campos obligatorios se identificaran con un asterisco (*)</b></p>

                <div class="form-group">
                    <label>numero *</label>
                    <input type="text" class="form-control" id="numero" name="numero" required="required">
                </div>

                <div class="form-group">
                    <label>Tipo*</label>
                    <select class="proveedoresc form-control" id="tipo" name="tipo" required="required">
                        <option value="">Seleccione</option>
                        <option value="1">Principal</option>
                        <option value="2">Adicional</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Saldo *</label>
                    <input type="number" class="form-control" id="saldo" name="saldo" required="required">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
  </div>
</div>
