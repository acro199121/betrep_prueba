
<div class="container">
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-primary" id="createTarjet">Nueva tarjeta</button>
        </div>
    </div>
</div>

<table id="table_tarjeta" data-pagination="true" data-side-pagination="server" data-search="true" data-toggle="table" data-url="<?php echo url('api/tarjeta/listTarjetas'); ?>">
    <thead>
        <tr>
            <th data-field="id" class="th20">#</th>
            <th data-field="numero">Nombre</th>
            <th data-field="tipo_name">Tarjeta</th>
            <th data-formatter="opcionesTarjetasFormatter" class="th20"></th>
        </tr>
    </thead>
</table>

@component('tarjetas.formTarjeta')
@endcomponent
