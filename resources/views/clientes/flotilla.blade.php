<div class="modal fade" id="flotillaModal" role="dialog" aria-labelledby="flotillaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Información de flotilla</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body mbsr">

            <div class="container">
                {{-- <div class="row">
                    <div class="col-9">
                         <div class="form-group">
                            <input type="text" class="form-control" id="buscar" name="barcar" placeholder="cliente / tarjeta">
                        </div>
                    </div>
                    <div class="col-3">
                         <button type="button" class="btn btn-info" id="buscarData">Buscar</button>
                    </div>
                </div> --}}

                <input type="hidden" name="id_cliente_search" id="id_cliente_search" value="">

                <div class="row mt-3">
                    <div class="col-4 ml-5 mr-4 bg-secondary text-white text-center disgrid">
                        <label >Cliente:</label>
                        <span id="clienteFlot"></span>
                    </div>
                    <div class="col-4 bg-secondary text-white text-center disgrid">
                        <label>Tarjeta:</label>
                        <span id="numero"></span>
                    </div>
                </div>

                <div class="row mt-4">

                    <div class="col-12">
                         <label for=""><strong>Tarjetas adicionales</strong></label>
                        <table id="table_flotilla" data-pagination="false" data-side-pagination="server" data-search="false" data-toggle="table" data-url="<?php echo url('api/clientes/listTarjAdicio'); ?>" data-query-params="queryParamsTableFlotilla">
                            <thead>
                                <tr>
                                    <th data-field="numero">Tarjeta</th>
                                    <th data-field="saldo">Saldo</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
