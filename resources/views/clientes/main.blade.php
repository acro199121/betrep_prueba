
<div class="container">
    <div class="row">
        <div class="col-12">
            <button type="button" class="btn btn-primary" id="createClient">Nuevo cliente</button>
            <button type="button" class="btn btn-success" id="excel">Excel</button>
        </div>
    </div>

    <div class="row">
        <div class="col-9"></div>
        <div class="col-3 mt-3 pr-0">
            <div class="form-group">
                <select class="evaluador form-control" id="estatus" name="estatus" required="required">
                    <option value="0">Estatus</option>
                    <option value="1">Activo</option>
                    <option value="2">Inactivo</option>
                    <option value="3">Suspendido</option>
                </select>
            </div>
        </div>
    </div>

</div>

<table id="table_cliente" data-pagination="true" data-side-pagination="server" data-search="true" data-toggle="table" data-url="<?php echo url('api/clientes/listClientes'); ?>" data-query-params="queryParamsTableCliente">
    <thead>
        <tr>
            <th data-field="id" class="th20">#</th>
            <th data-field="nombre">Nombre</th>
            <th data-field="numero">Tarjeta</th>
            <th data-field="saldo">Saldo total</th>
            <th data-field="estatus_name">Estatus</th>
            <th data-formatter="opcionesClientesFormatter" class="th20"></th>
        </tr>
    </thead>
</table>

@component('clientes.formCliente')
@endcomponent

@component('clientes.flotilla')
@endcomponent
