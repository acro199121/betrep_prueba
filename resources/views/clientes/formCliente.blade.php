<div class="modal fade" id="clienteModal" role="dialog" aria-labelledby="clienteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ingresar nuevo cliente</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form enctype="multipart/form-data" id="dataCliente" method="post" >
            <div class="modal-body mbsr">
                {{ csrf_field() }}

                <p><b>Los campos obligatorios se identificaran con un asterisco (*)</b></p>

                <div class="form-group">
                    <label>Nombre *</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Digite el nombre del proveedor" required="required">
                </div>

                <div class="form-group">
                    <label>Celular*</label>
                    <input type="number" class="form-control" id="phone" name="phone" placeholder="Digite numero de celular" required="required">
                </div>

                <div class="form-group">
                    <label>Correo electronico*</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Digite el correo electronico" required="required" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="el correo electronico no es correcto">
                </div>

                <div class="form-group">
                    <label>Sexo*</label>
                    <select class="proveedoresc form-control" id="sexo" name="sexo" required="required">
                        <option value="0">Seleccione</option>
                        <option value="M">Masculino</option>
                        <option value="F">Femenino</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Flotilla *</label>
                    <select class="evaluador form-control" id="flota" name="flota" required="required">
                        <option value="0">Seleccione</option>
                        <option value="1">Si</option>
                        <option value="2">No</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>RFC*</label>
                    <input type="number" class="form-control" id="rfc" name="rfc" placeholder="Digite numero RFC" required="required">
                </div>

                <div class="form-group" id="contentPlantilla">
                    <label>Tarjeta * (debe de seleccionar los datos de la flotilla para cargar las tarjetas)</label>

                    <select class="form-control" id="tarjeta" name="tarjeta[]" multiple="multiple" required="required">
                    </select>
                </div>

                <div class="form-group">
                    <label>Estatus *</label>
                    <select class="evaluador form-control" id="estatus" name="estatus" required="required">
                        <option value="0">Seleccione</option>
                        <option value="1">Activo</option>
                        <option value="2">Inactivo</option>
                        <option value="3">Suspendido</option>
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            <input type="hidden" name="id_cliente_edit" id="id_cliente_edit" value="">
        </form>
    </div>
  </div>
</div>
