@extends('layouts.app')

@section('content')

    <div class="container mt-5">
        <div class="row">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="client-tab" data-toggle="tab" href="#client" role="tab" aria-controls="client" aria-selected="true">Clientes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tar-tab" data-toggle="tab" href="#tar" role="tab" aria-controls="tar" aria-selected="false">Tarjetas</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
       <div class="row">
           <div class="col-12">
               <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active mt-5" id="client" role="tabpanel" aria-labelledby="client-tab">
                   @component('clientes.main')
                   @endcomponent
                </div>
                <div class="tab-pane fade mt-5" id="tar" role="tabpanel" aria-labelledby="tar-tab">
                    @component('tarjetas.main')
                    @endcomponent
                </div>
            </div>
           </div>
       </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/tarjetas/tarjeta.js')}}"></script>
    <script src="{{asset('js/clientes/clientes.js')}}"></script>
@endsection
