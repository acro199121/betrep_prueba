<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf_token" content="{{ csrf_token() }}" />

        <link rel="stylesheet" href="{{asset('css/app/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('css/app/bootstrap-table.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/app/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" href="{{asset('fontawesome/css/all.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/app/wizard.css')}}">
        <link rel="stylesheet" href="{{asset('css/app/bootstrap-datepicker3.min.css')}}" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    </head>
    <body>

        @yield('content')

        <script src="{{asset('js/app/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('js/app/popper.min.js')}}"></script>
        <script src="{{asset('js/app/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/app/bootstrap-table.min.js')}}"></script>
        <script src="{{asset('js/app/bootstrap-table-es-ES.min.js')}}"></script>
        <script src="{{asset('js/app/select2.min.js')}}"></script>
        <script src="{{asset('js/i18n/es.js')}}"></script>
        <script src="{{asset('js/app/custom.js')}}"></script>
        <script src="{{asset('js/app/chart.js@2.8.0')}}"></script>
        <script src="{{asset('js/app/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{asset('js/app/bootstrap-datepicker.es.min.js')}}"></script>
        @yield('scripts')
    </body>
</html>
