<?php

namespace App\Servics\Clientes;

use App\Models\Cliente;
use App\Models\TarjetaCliente;
use Illuminate\Support\Facades\DB;



class ClientesService
{

    /**
     * Calcula el saldo de todas las tarjetas del cliente
     */
    public function saldoTarjetas($id_cliente)
    {
        $query = TarjetaCliente::join('tarjetas', 'tarjeta_cliente.id_tarjeta', '=', 'tarjetas.id')
        ->where('id_cliente', $id_cliente)
        ->select(
            DB::raw('SUM(saldo) AS saldo')
        )
        ->get();

        return $query;
    }

    /**
     * Consultar la tarjeta principal del cliente
     * @param $id_client
     */
    public function tarjetaPrincipal($id_cliente)
    {
        try {
            $succes = false;
            $data = "No tiene una tarjeta principal asignada";

            $query = TarjetaCliente::join('tarjetas', 'tarjeta_cliente.id_tarjeta', '=', 'tarjetas.id')
                ->where('id_cliente', $id_cliente)
                ->where('tipo', 1)
                ->select(
                    'numero'
                )
                ->get()->first();

            if ($query) {
                $succes = true;
                $data = $query->numero;
            }

            $response = array(
                'success' => $succes,
                'numero' => $data
            );

            return $response;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Consulta la tabla de tarjetas
     */
    public function listarClientes()
    {
        try {
            $busqueda = $pagina = $cantidad = $estatus=  false;
            $data = [];

            if (isset($_GET['search']) && ($_GET['search'] != '')) {
                $busqueda = $_GET['search'];
            }
            if (isset($_GET['offset'])) {
                $pagina = $_GET['offset'];
            }
            if (isset($_GET['limit'])) {
                $cantidad = $_GET['limit'];
            }

            if (isset($_GET['estatus'])) {
                $estatus = $_GET['estatus'];
            }

            $query = Cliente::select(
                '*',
                DB::raw('
                    CASE
                        WHEN clientes.estatus = 1 THEN "Activo"
                        WHEN clientes.estatus = 2 THEN "Inactivo"
                        WHEN clientes.estatus = 3 THEN "Suspendido"
                        ELSE "No definido"
                    END AS estatus_name
                '),
            );

            if ($busqueda !== false) {
                $query->where("nombre", "like", "%".$busqueda."%");
            }

            if ($estatus != 0) {
                $query->where("clientes.estatus", $estatus);
            }

            $total = $query->count();

            if ($pagina !== false && $cantidad !== false) {
                $query->skip($pagina)->take($cantidad);
            }

            $rows = $query->get();

            foreach ($rows as $value) {
                $saldo = $this->saldoTarjetas($value['id']);
                $tj = $this->tarjetaPrincipal($value['id']);

                $data[] = array(
                    'id' => $value['id'],
                    'nombre' => $value['nombre'],
                    'numero' => $tj['numero'],
                    'saldo' => "$".$saldo[0]['saldo'],
                    'estatus_name' => $value['estatus_name']
                );
            }

            $response = array(
                'rows' => $data,
                'total' => $total
            );

            return $response;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Crea un cliente
     * @param REQUEST $request
     */
    public function crearCliente($request)
    {
        try {
            $cont = 0;
            $succes = false;
            $message = "No se pudo registrar la información del cliente";

            if($request->id_cliente_edit == 0){
                $query = new Cliente();
            }else{
                $query = Cliente::find($request->id_cliente_edit);
                $this->eliminarTarjetasAsignasdas($request->id_cliente_edit);
            }

            $query->nombre = $request->nombre;
            $query->celular = $request->phone;
            $query->correo = $request->email;
            $query->sexo = $request->sexo;
            $query->flotilla = $request->flota;
            $query->rfc = $request->rfc;
            $query->estatus = $request->estatus;
            $query->save();

            if ($query->id) {
                foreach ($request->tarjeta as $value) {
                    $query2 = new TarjetaCliente();
                    $query2->id_cliente = $query->id;
                    $query2->id_tarjeta = $value;
                    $query2->save();
                    if($query2){
                        $cont++;
                    }
                }

                if($cont == sizeof($request->tarjeta)){
                    $succes = true;
                    $message = "Cliente registrado";
                }
            }
            $response  = array(
                'success' => $succes,
                'message' => $message
            );
            return $response;
        } catch (\Throwable $th) {
            return $th;
        }
    }


    /**
     * Consulta las tarjetas de un cliente
     * @param int $id_cliente
     */
    public function getTarjetasClient($id_cliente){

        $data = [];

        $query = TarjetaCliente::join('tarjetas', 'tarjeta_cliente.id_tarjeta', '=', 'tarjetas.id')
        ->where('tarjeta_cliente.id_cliente', $id_cliente)
        ->select('tarjetas.*')
        ->get();

        foreach ($query as $value) {
            $data[] = array(
                'id' => $value['id'],
                'number' => ($value['tipo'] == 1) ? $value['numero'] . " (Principal)" : $value['numero'] . " (Adicional)"
            );
        }

        return $data;
    }

    /**
     * Consulta los datos de un cliente para modificar
     */
    public function getClienteData($id_cliente){
        try {
            $query = Cliente::where('id', $id_cliente)
            ->select('*')
            ->get()->first();

            if($query->id){

                $data = array(
                    'id' => $query->id,
                    'nombre' => $query->nombre,
                    'phone' => $query->celular,
                    'correo' => $query->correo,
                    'sexo' => $query->sexo,
                    'flota' => $query->flotilla,
                    'RFC' => $query->rfc,
                    'tarjetas' => $this->getTarjetasClient($id_cliente),
                    'estatus' => $query->estatus
                );

            }
            return $data;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Elimina las tarjetas asignadas
     * @param int $id_Cliente
     */
    public function eliminarTarjetasAsignasdas($id_cliente){
        try {
            $query = TarjetaCliente::where('id_cliente', $id_cliente)
            ->delete();
        } catch (\Throwable $th) {
            return $th;
        }

    }

    /**
     * Consultar tarjetas adicionales
     */
    public function tarjetasAdicionales()
    {
        try {

            $id_cliente =  false;
            $data = [];

            if (isset($_GET['cliente']) && ($_GET['cliente'] != 0)) {
                $id_cliente = $_GET['cliente'];
            }

            $query = TarjetaCliente::join('tarjetas', 'tarjeta_cliente.id_tarjeta', '=', 'tarjetas.id')
                ->where('tarjeta_cliente.id_cliente', $id_cliente)
                ->where('tarjetas.tipo', 2)
                ->select('tarjetas.*')
                ->get();

            foreach ($query as $value) {
                $data[] = array(
                    'numero' => $value['numero'],
                    'saldo' => "$" . $value['saldo'],
                );
            }

            $response = array(
                'rows' => $data,
            );

            return $response;
        } catch (\Throwable $th) {
            return $th;
        }


    }
}
