<?php

namespace App\Servics\Tarjetas;

use App\Models\Tarjeta;
use Illuminate\Support\Facades\DB;
use App\Servics\Clientes\ClientesService;



class TarjetaService
{

    /**
     * Consulta la tabla de tarjetas
     */
    public function listarTarjetas()
    {
        try {
            $busqueda = $pagina = $cantidad = false;

            if (isset($_GET['search']) && ($_GET['search'] != '')) {
                $busqueda = $_GET['search'];
            }
            if (isset($_GET['offset'])) {
                $pagina = $_GET['offset'];
            }
            if (isset($_GET['limit'])) {
                $cantidad = $_GET['limit'];
            }

            $query = Tarjeta::where('flag',1)
            ->select(
                '*',
                DB::raw('
                    CASE
                        WHEN tipo = 1 THEN "Principal"
                        ELSE "Adicional"
                    END AS tipo_name
                ')
            );

            if ($busqueda !== false) {
                $query->where("numero", $busqueda);
            }

            $total = $query->count();

            if ($pagina !== false && $cantidad !== false) {
                $query->skip($pagina)->take($cantidad);
            }

            $rows = $query->get();

            $response = array(
                'rows' => $rows,
                'total' => $total
            );

            return $response;
        } catch (\Throwable $th) {
            return $th;
        }


    }


    /**
     * Crea una tarjeta
     * @param $request
     * @return $response
     */
    public function crearTarjeta($request)
    {
        try {
            $succes = false;
            $message = "No se pudo guardar la información de la tarjeta";

            $query = new Tarjeta();
            $query->numero = $request->numero;
            $query->tipo = $request->tipo;
            $query->saldo = $request->saldo;
            $query->save();
            if ($query->id) {
                $succes = true;
                $message = "Datos guardados con exito";
            }
            $response  = array(
                'success' => $succes,
                'message' => $message
            );
            return $response;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Eliminado logico de una tarjeta
     */
    public function eliminarTarjeta($id)
    {
        try {
            $succes = false;
            $message = "No se pudo eliminar la tarjeta";

            $query = Tarjeta::where('id', $id)->update(['flag' => 0]);
            if($query){
                $succes = true;
                $message = "Tarjeta eliminada";
            }

            $response  = array(
                'success' => $succes,
                'message' => $message
            );
            return $response;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Metodo para consultar las tarjetas select
     * @param $request
     */
    public function getTarjetasSelect($request)
    {
        try {
            $data = [];
            $key = 0;
            $search = $request->serach;
            $flotilla = $request->flotilla;
            $id_cliente = $request->id_cliente;
            $obj = new ClientesService();
            $tarjetaPrincipal = $obj->tarjetaPrincipal($id_cliente);

            $query = Tarjeta::LeftJoin('tarjeta_cliente', 'tarjetas.id','=', 'tarjeta_cliente.id_tarjeta')
            ->whereNull('tarjeta_cliente.id_tarjeta')
            ->select('*');

            if($flotilla == 2 && $tarjetaPrincipal['success'] !== true){
                $query->where('tarjetas.tipo', 1);
            }else if($flotilla == 2 && $tarjetaPrincipal['success'] === true){
                $query->whereNotIn('tarjetas.tipo', [1,2]);
            }

            if ($flotilla == 1 && $tarjetaPrincipal['success'] === true) {
                $query->where('tarjetas.tipo', 2);
            }

            if ($search != "") {
                $query->where('tarjetas.numero', "like", "%" . $search . "%");
            }

            $query = $query->get();

            foreach ($query as $value) {

                if($value['tipo'] != $key){
                    $data[] = array(
                        'id' => $value['id'],
                        'number' => ($value['tipo'] == 1) ? $value['numero'] . " (Principal)" : $value['numero'] . " (Adicional)"
                    );
                    if($value['tipo'] == 1){
                        $key = $value['tipo'];
                    }
                }
            }

            return $data;
        } catch (\Throwable $th) {
            return $th;
        }
    }
}
