<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Servics\Clientes\ClientesService;

class ClientesExport implements FromArray, WithHeadings, WithEvents, ShouldAutoSize
{
    protected $obj;

    public function __construct()
    {
        $this->obj = new ClientesService();
    }

    public function headings(): array
    {
        return [
            '#',
            'NOMBRE',
            '#TARJETA PRINCIPAL',
            'SALDO',
            'ESTATUS',
        ];
    }

    public function array(): array
    {
        $resp = $this->obj->listarClientes();
        return $resp['rows'];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                // All headers - set font size to 14
                $cellRange = 'A1:W1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setName('Arial');
            },
        ];
    }
}
