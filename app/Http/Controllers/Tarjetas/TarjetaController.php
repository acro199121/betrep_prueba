<?php

namespace App\Http\Controllers\Tarjetas;

use App\Models\Tarjeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servics\Tarjetas\TarjetaService;

class TarjetaController extends Controller
{

    protected $obj;

    public function __construct()
    {
        $this->obj = new TarjetaService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resp = $this->obj->listarTarjetas();
        return response()->json($resp);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resp = $this->obj->crearTarjeta($request);
        return response()->json($resp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $resp = $this->obj->eliminarTarjeta($request->id);
        return response()->json($resp);
    }

    /**
     * Consulta los datos de una tarjeta para el select
     * @param  \Illuminate\Http\Request  $request
     */
    public function getTarjetasSelect(Request $request){
        $resp = $this->obj->getTarjetasSelect($request);
        return response()->json($resp);
    }
}
