<?php

namespace App\Http\Controllers\Clientes;

use App\Models\Cliente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servics\Clientes\ClientesService;
use App\Exports\ClientesExport;
use Maatwebsite\Excel\Facades\Excel;

class ClienteController extends Controller
{
    protected $obj;

    public function __construct()
    {
        $this->obj = new ClientesService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resp = $this->obj->listarClientes();
        return response()->json($resp);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resp = $this->obj->crearCliente($request);
        return response()->json($resp);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $resp = $this->obj->getClienteData($request->id);
        return response()->json($resp);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }

    /**
     * Metodo para exportar el excel de los clientes
     */
    public function export(Request $request)
    {
        return Excel::download(new ClientesExport(), 'clientes.xlsx');
    }

    /**
     * Consulta las tarjetas adicionales del cliente.
     *
     * @return \Illuminate\Http\Response
     */
    public function tarjetasAdicionales()
    {
        $resp = $this->obj->tarjetasAdicionales();
        return response()->json($resp);
    }
}
