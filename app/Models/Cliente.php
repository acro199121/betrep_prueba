<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cliente
 * 
 * @property int $id
 * @property string $nombre
 * @property string $celular
 * @property string $correo
 * @property string $sexo
 * @property int $flotilla
 * @property string $rfc
 * @property int $estatus
 * 
 * @property Collection|Tarjeta[] $tarjetas
 *
 * @package App\Models
 */
class Cliente extends Model
{
	protected $table = 'clientes';
	public $timestamps = false;

	protected $casts = [
		'flotilla' => 'int',
		'estatus' => 'int'
	];

	protected $fillable = [
		'nombre',
		'celular',
		'correo',
		'sexo',
		'flotilla',
		'rfc',
		'estatus'
	];

	public function tarjetas()
	{
		return $this->belongsToMany(Tarjeta::class, 'tarjeta_cliente', 'id_cliente', 'id_tarjeta');
	}
}
