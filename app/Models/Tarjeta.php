<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tarjeta
 * 
 * @property int $id
 * @property string $numero
 * @property int $tipo
 * @property bool $flag
 * 
 * @property Collection|Cliente[] $clientes
 *
 * @package App\Models
 */
class Tarjeta extends Model
{
	protected $table = 'tarjetas';
	public $timestamps = false;

	protected $casts = [
		'tipo' => 'int',
		'flag' => 'bool'
	];

	protected $fillable = [
		'numero',
		'tipo',
		'flag'
	];

	public function clientes()
	{
		return $this->belongsToMany(Cliente::class, 'tarjeta_cliente', 'id_tarjeta', 'id_cliente');
	}
}
