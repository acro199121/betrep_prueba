<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TarjetaCliente
 * 
 * @property int $id
 * @property int $id_cliente
 * @property int $id_tarjeta
 * 
 * @property Cliente $cliente
 * @property Tarjeta $tarjeta
 *
 * @package App\Models
 */
class TarjetaCliente extends Model
{
	protected $table = 'tarjeta_cliente';
	public $timestamps = false;

	protected $casts = [
		'id_cliente' => 'int',
		'id_tarjeta' => 'int'
	];

	protected $fillable = [
		'id_cliente',
		'id_tarjeta'
	];

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'id_cliente');
	}

	public function tarjeta()
	{
		return $this->belongsTo(Tarjeta::class, 'id_tarjeta');
	}
}
